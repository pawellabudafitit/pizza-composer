import {Component} from '@angular/core';
import {versions} from '../../../../environments/versions';

@Component({
  selector: 'dbs-version',
  template: `
    <p>
      {{ 'Version:' + versions.version }} &nbsp;
      {{ 'Revision:' + versions.revision }} &nbsp;
      {{ 'Branch:' + versions.branch }} &nbsp;
      {{ 'Date:' + versions.date }} &nbsp;
    </p>
  `,
  styleUrls: ['./version.component.scss']
})
export class VersionComponent{
  versions = versions;
  constructor() { }
}
