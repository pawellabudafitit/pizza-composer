import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CoreComponent} from './containers/core/core.component';
import {RouterModule, Routes} from '@angular/router';
import {GuardedComponent} from './containers/guarded/guarded.component';
import {AuthGuard} from '../auth/guard/auth.guard';
import {FakeAuthComponent} from '../auth/containers/fake-auth/fake-auth.component';
import {AuthModule} from '../auth/auth.module';
import {RoleGuard} from '../auth/guard/role.guard';
import {ScopeGuard} from '../auth/guard/scope.guard';
import {ListService} from '../list/list.service';
import {HttpClientModule} from '@angular/common/http';
import {ListModule} from '../list/list.module';
import {ListComponent} from '../list/containers/list.component';
import {VersionComponent} from './components/version/version.component';

const routes: Routes = [
  {
    path: '', component: CoreComponent, children: [
      {
        path: '', component: GuardedComponent
      },
      {
        path: 'guarded', component: GuardedComponent, canActivate: [AuthGuard]
      },
      {
        path: 'list', component: ListComponent,
      },
      {
        path: 'roleTestForAdmin', component: GuardedComponent, canActivate: [AuthGuard, RoleGuard, ScopeGuard],
        data: {
          roles: ['TEST'],
          scopes: ['ADMIN']
        }
      },
      {
        path: 'roleTestForSuperAdmin', component: GuardedComponent, canActivate: [AuthGuard, RoleGuard, ScopeGuard],
        data: {
          roles: ['TEST'],
          scopes: ['SUPER_ADMIN']
        }
      },
      {
        path: 'roleWeird', component: GuardedComponent, canActivate: [AuthGuard, RoleGuard],
        data: {
          roles: ['WEIRD']
        }
      },
      {
        path: 'roleTestOrWeird', component: GuardedComponent, canActivate: [AuthGuard, RoleGuard],
        data: {
          roles: ['TEST', 'WEIRD']
        }
      },
      {
        path: 'roleTestAndWeird', component: GuardedComponent, canActivate: [AuthGuard, ScopeGuard],
        data: {
          scopes: ['USER', 'ADMIN', 'SUPER_ADMIN']
        }
      },
      {
        path: 'fake-auth', component: FakeAuthComponent
      }
    ]
  },
];

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule.forChild(routes),
    ListModule,
    AuthModule
  ],
  declarations: [
    CoreComponent,
    GuardedComponent,
    VersionComponent
  ],
  providers: [
    ListService
  ]
})
export class CoreModule { }
