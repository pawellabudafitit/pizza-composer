import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'dbs-guarded',
  template: `
    <p>
      guarded works!
    </p>
  `,
  styleUrls: ['./guarded.component.scss']
})
export class GuardedComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
