import {Component, OnInit} from '@angular/core';
import {getLogin, isAuthenticated} from '../../../auth/store/auth.selector';
import {Observable} from 'rxjs/Observable';
import {Store} from '@ngrx/store';
import {RouterState} from '../../../app-store/router/router.model';
import {environment} from '../../../../environments/environment';
import {getNotifications} from '../../../notification/store/notification.selector';
import {NotificationType} from '../../../notification/store/notification.model';
import {NotificationAddAction} from '../../../notification/store/notification.action';
import {JwtLogoutAction} from '../../../auth/store/auth.action';

@Component({
  selector: 'dbs-app',
  template: `
    <h3>
      {{ isProduction ? 'Production' : 'Development' }}
    </h3>
    <p>
      {{ login$ | async }} <a *ngIf="isAuthenticated$ | async" (click)="logout()">Logout</a>
    </p>
    <p>
      <a [routerLink]="['/']">Main</a>
      <a [routerLink]="['/guarded']">Guarded</a>
      <a [routerLink]="['/list']">List</a>
      <a [routerLink]="['/roleTestForAdmin']">Test for ADMIN</a>
      <a [routerLink]="['/roleTestForSuperAdmin']">Test for SUPER_ADMIN</a>
      <a [routerLink]="['/roleWeird']">Weird</a>
      <a [routerLink]="['/roleTestOrWeird']">Test or Weird</a>
      <a [routerLink]="['/roleTestAndWeird']">Test and Weird</a>
    </p>
    <p>
      <a (click)="addNotification()">Add notification</a>
      <router-outlet></router-outlet>
    </p>
    <dbs-version></dbs-version>
  `,
  styleUrls: ['./core.component.scss']
})
export class CoreComponent implements OnInit {
  isProduction = environment.production;

  login$: Observable<string>;
  isAuthenticated$: Observable<boolean>;

  constructor(private store: Store<RouterState>) {}

  ngOnInit(): void {
    this.login$ = this.store.select(getLogin);
    this.isAuthenticated$ = this.store.select(isAuthenticated);

    this.store.select(getNotifications).subscribe(
      notifications => console.log(notifications)
    );
  }

  addNotification(): void {
    this.store.dispatch(new NotificationAddAction({
      type: NotificationType.INFO,
      message: 'Hihi'
    }));
  }

  logout(): void {
    this.store.dispatch(new JwtLogoutAction({path:['/']}));
  }

}
