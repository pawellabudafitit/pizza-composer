import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {GoParams} from '../../app-store/router/router.model';
import {ActivatedRouteSnapshot, convertToParamMap} from '@angular/router';

@Injectable()
export class UrlUtil {

  private jwtTokenQueryParamName = environment.urlTokenName;

  constructor() { }

  removeJwtTokenFromUri(goParams: GoParams): GoParams {
    const queryParams = { ...goParams.queryParams };
    delete queryParams[this.jwtTokenQueryParamName];
    return {
      ...goParams,
      queryParams: queryParams
    };
  }

  convert(snapshot: ActivatedRouteSnapshot): GoParams {
    const path = snapshot.url.map(urlSegment => urlSegment.path);
    const queryParams = snapshot.queryParams;
    return { path, queryParams };
  }

  toUri(goParams: GoParams) {
    const path = '/' + goParams.path.join('/');
    const paramMap = convertToParamMap(goParams.queryParams);
    const queryParams = paramMap.keys.map((param) => param + '=' + paramMap.get(param)).join('&');
    return path + (queryParams ? '?' + queryParams : '');
  }

}
