import {Component} from '@angular/core';

@Component({
  selector: 'dbs-root',
  template: `
    <router-outlet></router-outlet>
  `
})
export class AppComponent {}

@Component({
  selector: 'dbs-page-not-found',
  template: `
    <p>Could not find the page</p>
  `
})
export class PageNotFoundComponent {}
