import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {list} from '../store/list.selector';
import {Observable} from 'rxjs/Observable';
import {ListState, PageRequest} from '../store/list.model';
import {ListPage} from '../store/list.action';
import {map} from 'rxjs/operators';
import {Go} from '../../app-store/router/router.action';
import {getRouterQueryParams} from '../../app-store/router/router.selector';
import {deserializeSort, printSort} from '../list.utils';

/**
 * It's important to use subscribe to observables in templates.
 * Angular unsubscribes every observable automatically when navigate away.
 *
 * So instead of doing it this way: observable$.subscribe(() => doSth()) do it using 'async' pipe in template:
 * [input]="observable$ | async"
 *
 * Unless you really know what are you doing ;)
 */
@Component({
  selector: 'sc-list',
  template: `
    <sc-paged-list [listState]="list$ | async"
                   (sortEmitter)="routeToPage($event)"
                   [routerPageRequest]="routerPageRequest$ | async"
                   (listRequestEmitter)="loadPage($event)"></sc-paged-list>
    <sc-pagination *ngIf="(list$ | async).current != null"
                   [currentList]="(list$ | async).current"
                   (selectPageEvent)="routeToPage($event)"></sc-pagination>

    <!--<dbs-infinite-list [listState]="list$ | async" (loadMoreEmitter)="loadMore($event)"></dbs-infinite-list>-->
  `,
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  list$: Observable<ListState>;
  routerPageRequest$: Observable<PageRequest>;

  constructor(private store: Store<ListState>) {
  }

  ngOnInit() {
    this.routerPageRequest$ = this.store.select(getRouterQueryParams).pipe(
      map(queryParams => ({
        page: parseInt(queryParams.page || 1, 10),
        size: parseInt(queryParams.size || 5, 10),
        ...(queryParams.sort ? {sort: deserializeSort(queryParams.sort)} : {})
      }))
    );

    this.list$ = this.store.select(list);
  }

  routeToPage(request: PageRequest) {
    this.store.dispatch(new Go({
      path: ['/list'],
      queryParams: {
        page: request.page,
        size: request.size,
        ...(request.sort ? {sort: printSort(request.sort)} : {})
      }
    }));
  }

  loadPage(request: PageRequest) {
    this.store.dispatch(new ListPage(request));
  }

  // loadMore(page: number) {
  //   this.store.dispatch(new ListPage({page, size: 5, listType: ListType.INFINITE}));
  // }

}
