import {Sort} from './store/list.model';

import {filter, find, isMatch} from 'lodash';

export function printSort(sort: Sort[]): string[] {
  return sort.map(sortField => sortField.property + ',' + (sortField.asc ? 'asc' : 'desc'));
}

export function deserializeSort(sort: string | string[]): Sort[] {
  const deserialize = sortValue => {
    const sortField = sortValue.split(',');
    return {
      property: sortField[0],
      asc: 'asc' === sortField[1]
    };
  };
  if (Array.isArray(sort)) {
    return sort.map(deserialize);
  } else {
    return [deserialize(sort)];
  }
}

export function putSort(sort: Sort, sortList: Sort[]): Sort[] {
  if (sortList) {
    const sortsWithoutTheSpecifiedOne =
      filter(sortList, sortElem => sortElem.property !== sort.property);
    return [sort, ...sortsWithoutTheSpecifiedOne];
  }
  return [sort];
}

export function findByProperty(list: any[], property: string) {
  return find(list, sort => isMatch(sort, {property}));
}
