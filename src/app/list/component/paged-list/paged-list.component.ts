import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {ListState, PageRequest} from '../../store/list.model';
import {findByProperty, putSort} from '../../list.utils';


@Component({
  selector: 'sc-paged-list',
  template: `
    <div *ngIf="listState.current != null">
      <div class="head">
        <div [ngClass]="getSort('id')" (click)="sort('id')">ID</div>
        <div [ngClass]="getSort('name')" (click)="sort('name')">NAME</div>
        <div [ngClass]="getSort('description')" (click)="sort('description')">DESCRIPTION</div>
      </div>
      <div *ngFor="let item of listState.current.items" class="row">
        <div>{{item.id}}</div>
        <div>{{item.name}}</div>
        <div>{{item.description}}</div>
      </div>
    </div>
  `,
  styleUrls: ['./paged-list.component.scss']
})
export class PagedListComponent implements OnInit, OnChanges {


  @Input()
  listState: ListState;

  @Input()
  private routerPageRequest: PageRequest;

  @Output()
  private sortEmitter = new EventEmitter<PageRequest>();

  @Output()
  private listRequestEmitter = new EventEmitter<PageRequest>();

  constructor() { }

  ngOnInit() {
  }
  ngOnChanges(changes: SimpleChanges): void {
    const queryParamsChange = changes['routerPageRequest'];
    if (queryParamsChange) {
      this.listRequestEmitter.emit(<PageRequest>(queryParamsChange.currentValue));
    }
  }
  getSort(property: string): string {
    const sortFound = findByProperty(this.listState.current.sort, property);
    return sortFound ? (sortFound.asc ? 'asc' : 'desc') : undefined;
  }

  sort(property: string) {
    const sort = findByProperty(this.listState.current.sort, property);
    const request = {
      page: this.listState.current.page,
      size: this.listState.current.size,
      sort: putSort((sort ? {...sort, asc: !sort.asc} : {property, asc: true}), this.listState.current.sort)
    };
    this.sortEmitter.emit(request);
  }

}
