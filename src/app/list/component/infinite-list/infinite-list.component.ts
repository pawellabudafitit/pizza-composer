import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ListState} from '../../store/list.model';

@Component({
  selector: 'sc-infinite-list',
  template: `
    <div *ngIf="listState.current != null">
      <div class="head">
        <div>ID</div>
        <div>NAME</div>
        <div>DESCRIPTION</div>
      </div>
      <div *ngFor="let item of listState.current.items" class="row">
        <div>{{item.id}}</div>
        <div>{{item.name}}</div>
        <div>{{item.description}}</div>
      </div>
      <a *ngIf="listState.current.hasNext"
         class="load-more"
         (click)="loadMore(listState.current.page+1)">Load more</a>
    </div>
  `,
  styleUrls: ['./infinite-list.component.scss']
})
export class InfiniteListComponent implements OnInit {

  @Input()
  listState: ListState;

  @Output()
  private loadMoreEmitter = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  loadMore(page: number) {
    this.loadMoreEmitter.emit(page);
  }

}
