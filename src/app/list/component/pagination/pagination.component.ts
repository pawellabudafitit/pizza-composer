import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PageRequest, PageResponse} from '../../store/list.model';

@Component({
  selector: 'sc-pagination',
  template: `
    <a *ngFor="let page of getPages()"
       class="page-link"
       [ngClass]="{'active': page === currentList.page-1}"
       (click)="selectPage(page+1)">{{page + 1}}</a>
  `,
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {

  @Input() currentList: PageRequest & PageResponse;

  @Output() selectPageEvent = new EventEmitter<PageRequest>();

  constructor() {
  }

  ngOnInit() {
  }

  getPages() {
    return Array.from(Array(Math.ceil(this.currentList.totalElements / this.currentList.size)).keys());
  }

  selectPage(page: number) {
    this.selectPageEvent.emit({
      page,
      size: this.currentList.size,
      listType: this.currentList.listType,
      sort: this.currentList.sort
    });
  }

}
