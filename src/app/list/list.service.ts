import {HttpClient} from '@angular/common/http';
import {PageRequest, PageResponse} from './store/list.model';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {environment} from '../../environments/environment';
import {printSort} from './list.utils';

@Injectable()
export class ListService {

  private backendUrl = environment.backendUrl;

  constructor(private httpClient: HttpClient) {}

  get(request: PageRequest): Observable<PageResponse> {
    return this.httpClient.get<PageResponse>(this.backendUrl + '/list', {
      params: {
        page: request.page.toString(),
        size: request.size.toString(),
        ...(request.sort ? {sort: printSort(request.sort)} : {})
      }
    });
  }

}
