// Request
export enum ListType {
  PAGED, INFINITE
}
export interface Sort {
  property: string;
  asc: boolean;
}

export interface PageRequest {
  page: number;
  size: number;
  listType?: ListType;
  sort?: Sort[];
}

// Response
export interface PageResponse {
  items: any[];
  totalElements: number;
  hasNext: boolean;
}

export interface ListState {
  current: PageResponse & PageRequest;
  next: PageRequest;

  loading: boolean;
}
