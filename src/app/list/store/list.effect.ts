import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {LIST_PAGE, ListAction, ListPage, ListPageFail, ListPageSuccess} from './list.action';
import {catchError, map, switchMap} from 'rxjs/operators';
import {ListService} from '../list.service';
import {of} from 'rxjs/observable/of';

@Injectable()
export class ListEffects {

  constructor(private actions$: Actions,
              private listService: ListService) {
  }

  @Effect()
  load$ = this.actions$.pipe(
    ofType(LIST_PAGE),
    switchMap<ListPage, ListAction>(({request}: ListPage) => this.listService.get(request)
      .pipe(
        map((response) => new ListPageSuccess(response)),
        catchError(() => of(new ListPageFail()))
      )
    )
  );

}
