import {Action} from '@ngrx/store';
import {ListType, PageRequest, PageResponse} from './list.model';

export const LIST_PAGE = '[List] Page action';
export const LIST_PAGE_SUCCESS = '[List] Page action success';
export const LIST_PAGE_FAIL = '[List] Page action fail';

export const defaultRequest: PageRequest = {page: 1, size: 5, listType: ListType.PAGED};

export class ListPage implements Action {
  readonly type = LIST_PAGE;
  constructor(public request: PageRequest = defaultRequest) {}
}
export class ListPageSuccess implements Action {
  readonly type = LIST_PAGE_SUCCESS;
  constructor(public response: PageResponse) {}
}
export class ListPageFail implements Action {
  readonly type = LIST_PAGE_FAIL;
}

export type ListAction = ListPage | ListPageSuccess | ListPageFail;
