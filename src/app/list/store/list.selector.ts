import {createFeatureSelector} from '@ngrx/store';
import {ListState} from './list.model';

export const list = createFeatureSelector<ListState>('list');
