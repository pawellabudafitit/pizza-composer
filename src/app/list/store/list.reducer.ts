import {LIST_PAGE, LIST_PAGE_FAIL, LIST_PAGE_SUCCESS, ListAction, ListPage, ListPageSuccess} from './list.action';
import {ListState, ListType, PageResponse} from './list.model';


export const initialListState: ListState = {
  current: null,
  next: null,

  loading: false
};

export function listReducer(state = initialListState, action: ListAction) {
  switch (action.type) {
    case LIST_PAGE: {
      const listPage = <ListPage> action;
      return {
        current: state.current,
        next: listPage.request,
        loading: true
      };
    }
    case LIST_PAGE_SUCCESS: {
      const listPageSuccess = <ListPageSuccess> action;
      return {
        current: calculateCurrentState(state, listPageSuccess.response),
        next: null,
        loading: false
      };
    }
    case LIST_PAGE_FAIL: {
      return {
        current: state.current,
        next: null,
        loading: false
      };
    }

    default: {
      return state;
    }
  }
}

function calculateCurrentState(previousState: ListState, response: PageResponse) {
  switch (previousState.next.listType) {
    case ListType.PAGED: {
      return { ...previousState.next, ...response };
    }
    case ListType.INFINITE: {
      return {
        ...previousState.next,
        items: [
          ...(previousState.current ? previousState.current.items : []),
          ...response.items],
        totalElements: response.totalElements,
        hasNext: response.hasNext
      };
    }
    default: {
      return { ...previousState.next, ...response };
    }
  }
}
