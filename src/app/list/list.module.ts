import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EffectsModule} from '@ngrx/effects';
import {StoreModule} from '@ngrx/store';
import {listReducer} from './store/list.reducer';
import {ListEffects} from './store/list.effect';
import {HttpClientModule} from '@angular/common/http';
import {ListComponent} from './containers/list.component';
import {PagedListComponent} from './component/paged-list/paged-list.component';
import {InfiniteListComponent} from './component/infinite-list/infinite-list.component';
import {PaginationComponent} from './component/pagination/pagination.component';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    StoreModule.forFeature('list', listReducer),
    EffectsModule.forFeature([ListEffects])
  ],
  declarations: [
    ListComponent,
    PagedListComponent,
    PaginationComponent,
    InfiniteListComponent
  ]
})
export class ListModule { }
