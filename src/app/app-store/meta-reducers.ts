import {storeFreeze} from 'ngrx-store-freeze';
import {MetaReducer} from '@ngrx/store';
import {logger} from './router/router.reducer';
import {environment} from '../../environments/environment';
import {RouterState} from './router/router.model';
import {localStorageSyncReducer} from './local-storage/local-storage.reducer';

/**
 * By default, @ngrx/store uses combineReducers with the reducer map to compose
 * the root meta-reducer. To add more meta-reducers, provide an array of meta-reducers
 * that will be composed to form the root meta-reducer.
 */
export const metaReducers: MetaReducer<RouterState>[] = !environment.production
  ? [logger, storeFreeze, localStorageSyncReducer]
  : [localStorageSyncReducer];
