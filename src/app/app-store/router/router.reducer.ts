import {ActivatedRouteSnapshot, Params, RouterStateSnapshot} from '@angular/router';

import {routerReducer, RouterStateSerializer} from '@ngrx/router-store';
import {RouterState} from './router.model';
import {ActionReducer, ActionReducerMap} from '@ngrx/store';

export const routerReducerMap: ActionReducerMap<RouterState> = {
  router: routerReducer
};

export function logger(reducer: ActionReducer<RouterState>): ActionReducer<RouterState> {

  return function(state: RouterState, action: any): RouterState {
    console.log('state', state);
    console.log('action', action);

    return reducer(state, action);
  };
}

export interface RouterStateUrl {
  url: string;
  queryParams: Params;
  params: Params;
}

export class CustomRouterStateSerializer implements RouterStateSerializer<RouterStateUrl> {
  serialize(routerState: RouterStateSnapshot): RouterStateUrl {
    const {url} = routerState;
    const {queryParams} = routerState.root;

    let state: ActivatedRouteSnapshot = routerState.root;
    while (state.firstChild) {
      state = state.firstChild;
    }
    const {params} = state;

    return {url, queryParams, params};
  }
}
