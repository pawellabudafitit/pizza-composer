import {createFeatureSelector, createSelector} from '@ngrx/store';
import {RouterReducerState} from '@ngrx/router-store';
import {RouterStateUrl} from './router.reducer';

import {environment} from '../../../environments/environment';

export const getRouterReducerState = createFeatureSelector<RouterReducerState<RouterStateUrl>>('router');
export const getRouterStateUrl = createSelector(getRouterReducerState, reducerState => reducerState.state);
export const getRouterQueryParams = createSelector(getRouterStateUrl, routerStateUrl => routerStateUrl.queryParams);
export const getRouterUrl = createSelector(getRouterStateUrl, routerStateUrl => routerStateUrl.url);

export const getJwtQueryParam = createSelector(
  getRouterQueryParams, queryParams => queryParams[environment.urlTokenName]
);
