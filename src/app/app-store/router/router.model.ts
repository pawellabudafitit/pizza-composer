import {RouterStateUrl} from './router.reducer';
import {RouterReducerState} from '@ngrx/router-store';
import {Params} from '@angular/router';

export interface RouterState {
  router: RouterReducerState<RouterStateUrl>;
}

export interface GoParams {
  readonly path?: string[];
  readonly queryParams?: Params;
  readonly strictUrl?: string;
}
