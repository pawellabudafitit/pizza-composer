import {Inject, Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {DOCUMENT, Location as AngularLocation} from '@angular/common';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {map, tap} from 'rxjs/operators';
import {BACK, FORWARD, Go, GO, STAY} from './router.action';
import {GoParams} from './router.model';

@Injectable()
export class RouterEffects {

  constructor(
    private actions$: Actions,
    private router: Router,
    private location: AngularLocation,
    @Inject(DOCUMENT) private document: Document) {
  }

  @Effect({dispatch: false})
  go$ = this.actions$.pipe(
    ofType(GO),
    map<Go, GoParams>(action => action.params),
    tap(({path, queryParams, strictUrl}: GoParams) => {
        if (!strictUrl) {
          this.router.navigate(path, {queryParams});
        } else {
          this.document.location.href = strictUrl;
        }
      }
    ));

  @Effect({dispatch: false})
  stay$ = this.actions$.pipe(
    ofType(STAY)
  );

  @Effect({dispatch: false})
  back$ = this.actions$.pipe(
    ofType(BACK),
    tap(() => this.location.back())
  );

  @Effect({dispatch: false})
  forward$ = this.actions$.pipe(
    ofType(FORWARD),
    tap(() => this.location.forward())
  );

}
