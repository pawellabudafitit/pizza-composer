import {Action} from '@ngrx/store';
import {GoParams} from './router.model';

export const GO = '[Router] Go';
export const STAY = '[Router] Stay';
export const BACK = '[Router] Back';
export const FORWARD = '[Router] Forward';

export class Go implements Action {
  readonly type = GO;

  constructor(public params: GoParams) {}
}

export class Stay implements Action {
  readonly type = STAY;
  constructor() {}
}

export class Back implements Action {
  readonly type = BACK;
}

export class Forward implements Action {
  readonly type = FORWARD;
}

export type RouterAction = | Go | Stay | Back | Forward;
