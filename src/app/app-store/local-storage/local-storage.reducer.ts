import {localStorageSync} from 'ngrx-store-localstorage';
import {ActionReducer} from '@ngrx/store';
import {AuthState, initialState} from '../../auth/store/auth.reducer';

export function localStorageSyncReducer(reducer: ActionReducer<any>): ActionReducer<any> {
  const keys = [{
    auth: {
      serialize: (authState: AuthState) => authState.jwt,
      deserialize: (jwt: string) => ({ ...initialState, jwt })
    }
  }];
  return localStorageSync({
    keys: keys,
    storageKeySerializer: () => 'jwt',
    rehydrate: true,
    removeOnUndefined: true
  })(reducer);
}
