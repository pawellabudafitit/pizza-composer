import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {RouterModule, Routes} from '@angular/router';
import {RouterStateSerializer, StoreRouterConnectingModule} from '@ngrx/router-store';

import {CustomRouterStateSerializer, routerReducerMap} from './app-store/router/router.reducer';
import {AuthModule} from './auth/auth.module';
import {environment} from '../environments/environment';
import {NotificationModule} from './notification/notification.module';
import {AppComponent, PageNotFoundComponent} from './app.components';
import {SharedModule} from './shared/shared.module';
import {metaReducers} from './app-store/meta-reducers';
import {RouterEffects} from './app-store/router/router.effect';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {JwtInterceptor} from './auth/auth.interceptor';


// console.log all actions
const routes: Routes  = [
  // {
  //   path: 'fun', component: FunComponent
  // },
  {
    path: '', loadChildren: './core/core.module#CoreModule'
  },
  {
    path: '**', component: PageNotFoundComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    StoreModule.forRoot(routerReducerMap, { metaReducers }),
    StoreRouterConnectingModule.forRoot({
      stateKey: 'router'
    }),
    EffectsModule.forRoot([RouterEffects]),
    StoreDevtoolsModule.instrument({
      name: 'NgRx Book Store DevTools',
      logOnly: environment.production
    }),
    SharedModule.forRoot(),
    AuthModule.forRoot(),
    NotificationModule
  ],
  providers: [
    { provide: RouterStateSerializer,
      useClass: CustomRouterStateSerializer
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
