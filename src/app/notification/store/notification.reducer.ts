import {NOTIFICATION_ADD_ACTION, NotificationAction} from './notification.action';
import {NotificationsState} from './notification.model';

const initialState: NotificationsState = {
  list: []
};

export function notificationsReducer(state = initialState, action: NotificationAction) {
  switch (action.type) {
    case NOTIFICATION_ADD_ACTION: {
      return {
        list: [action.notification, ...state.list]
      };
    }
    default:
      return state;
  }
}
//
// export const notificationReducersMap: ActionReducerMap<NotificationsFeatureState> = {
//   notifications: notificationsReducer
// };
