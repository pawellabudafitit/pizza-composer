import {Action} from '@ngrx/store';
import {Notification} from './notification.model';

export const NOTIFICATION_ADD_ACTION = '[Notification] Add action';

export class NotificationAddAction implements Action {
  readonly type = NOTIFICATION_ADD_ACTION;
  constructor(public notification: Notification) {}
}

export type NotificationAction = NotificationAddAction;
