import {createFeatureSelector, createSelector} from '@ngrx/store';
import {NotificationsState} from './notification.model';

export const getNotificationsFeatureState = createFeatureSelector<NotificationsState>('notifications');
export const getNotifications = createSelector(getNotificationsFeatureState, state => state.list);
