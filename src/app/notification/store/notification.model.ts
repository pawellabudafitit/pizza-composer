export enum NotificationType {
  ERROR, INFO
}

export interface Notification {
  type: NotificationType;
  message: string;
}

export interface NotificationsState {
  list: Notification[];
}

export interface NotificationsFeatureState {
  notifications: NotificationsState;
}
