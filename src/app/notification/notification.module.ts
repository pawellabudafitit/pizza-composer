import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {StoreModule} from '@ngrx/store';
import {notificationsReducer} from './store/notification.reducer';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('notifications', notificationsReducer)
  ],
  declarations: []
})
export class NotificationModule { }
