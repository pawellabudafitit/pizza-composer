import {Injectable} from '@angular/core';
import {JwtHelperService} from '@auth0/angular-jwt';
import {JwtAuthPayload} from '../store/auth.reducer';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import {ErrorObservable} from 'rxjs/observable/ErrorObservable';
import {map} from 'rxjs/operators';

declare var localStorage: Storage;

@Injectable()
export class AuthService {

  private jwtHelperService: JwtHelperService;

  constructor() {
    this.jwtHelperService = new JwtHelperService();
  }

  decodeToken(jwtToken: string): Observable<JwtAuthPayload> {
    try {
      return this.checkToken(jwtToken)
        .pipe(
          map((valid) => {
            if (valid) {
              return <JwtAuthPayload>this.jwtHelperService.decodeToken(jwtToken);
            }
            throw new Error();
          })
        );
    } catch (error) {
      return new ErrorObservable('Not authenticated via token');
    }
  }

  // TODO: should call sso to check token
  checkToken(jwtToken: string): Observable<boolean> {
    return of(this.isTokenLive(jwtToken));
  }

  isTokenLive(jwtToken: string): boolean {
    try {
      return jwtToken && !this.jwtHelperService.isTokenExpired(jwtToken);
    } catch (error) {
      return false;
    }
  }

}
