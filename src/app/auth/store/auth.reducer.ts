import {
  JWT_LOGIN_ACTION_FAIL,
  JWT_LOGIN_ACTION_INIT,
  JWT_LOGIN_ACTION_SUCCESS,
  JWT_LOGOUT_ACTION,
  JwtLoginAction,
  JwtLoginActionSuccess
} from './auth.action';

export interface AuthFeatureState {
  auth: AuthState;
}

export interface UserDetails {
  login: string;
  email: string;
}

export interface JwtAuthPayload {
  exp: number;
  userDetails: UserDetails;
  roles: string[];
  scopes: string[];
}

export interface AuthState {
  payload: {
    exp: number;
    userDetails: UserDetails;
    roles: string[];
    scopes: string[];
  };

  authenticated: boolean;
  isAuthenticating: boolean;
  jwt?: string;
  error?: any;
}

export const initialState: AuthState = {
  payload: {
    exp: 0,
    userDetails: {
      login: 'anonymous',
      email: ''
    },
    roles: [],
    scopes: []
  },
  authenticated: false,
  isAuthenticating: false
};

export function authReducer(state = initialState, action: JwtLoginAction): AuthState {
  switch (action.type) {
    case JWT_LOGIN_ACTION_INIT: {
      return {
        ...state,
        authenticated: false,
        isAuthenticating: true
      };
    }
    case JWT_LOGIN_ACTION_SUCCESS: {
      const loginSuccessAction = <JwtLoginActionSuccess>action;
      return {
        payload: loginSuccessAction.jwtAuthPayload,
        authenticated: true,
        isAuthenticating: false,
        jwt: loginSuccessAction.jwtToken
      };
    }
    case JWT_LOGIN_ACTION_FAIL: {
      return initialState;
    }
    case JWT_LOGOUT_ACTION: {
      return initialState;
    }
  }
  return state;
}
