import {Action} from '@ngrx/store';
import {JwtAuthPayload} from './auth.reducer';
import {GoParams} from '../../app-store/router/router.model';

export const JWT_LOGIN_ACTION_INIT = '[Auth] JWT Login action init';
export const JWT_LOGIN_ACTION_SUCCESS = '[Auth] JWT Login action success';
export const JWT_LOGIN_ACTION_FAIL = '[Auth] JWT Login action fail';

export const JWT_CHECK_ACTION = '[Auth] JWT Check action';

export const JWT_LOGOUT_ACTION = '[Auth] JWT Logout action';

export class JwtLoginActionInit implements Action {
  readonly type = JWT_LOGIN_ACTION_INIT;

  constructor(public goParams: GoParams, public jwtToken: string) {
  }
}

export class JwtLoginActionSuccess implements Action {
  readonly type = JWT_LOGIN_ACTION_SUCCESS;

  constructor(public jwtAuthPayload: JwtAuthPayload, public goParams: GoParams, public jwtToken: string) {
  }
}

export class JwtLoginActionFail implements Action {
  readonly type = JWT_LOGIN_ACTION_FAIL;

  constructor(public goParams: GoParams) {
  }
}

export class JwtCheckAction implements Action {
  readonly type = JWT_CHECK_ACTION;

  constructor(public goParams: GoParams, public jwtToken: string) {
  }
}

export class JwtLogoutAction implements Action {
  readonly type = JWT_LOGOUT_ACTION;

  constructor(public redirectPath: GoParams) {
  }
}

export type JwtLoginAction =
  | JwtLoginActionInit
  | JwtLoginActionSuccess
  | JwtLoginActionFail
  | JwtCheckAction
  | JwtLogoutAction;
