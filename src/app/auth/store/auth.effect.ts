import {Inject, Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {
  JWT_CHECK_ACTION,
  JWT_LOGIN_ACTION_FAIL,
  JWT_LOGIN_ACTION_INIT,
  JWT_LOGIN_ACTION_SUCCESS,
  JWT_LOGOUT_ACTION,
  JwtCheckAction,
  JwtLoginAction,
  JwtLoginActionFail,
  JwtLoginActionInit,
  JwtLoginActionSuccess,
  JwtLogoutAction
} from './auth.action';
import {of} from 'rxjs/observable/of';
import {catchError, map, switchMap} from 'rxjs/operators';
import {AuthService} from '../service/auth.service';
import {DOCUMENT} from '@angular/common';
import {environment} from '../../../environments/environment';
import {Go, RouterAction, Stay} from '../../app-store/router/router.action';
import {GoParams} from '../../app-store/router/router.model';
import {UrlUtil} from '../../shared/utils/url.util';
import * as URI from 'urijs';

@Injectable()
export class AuthEffects {

  private ssoLoginPageUrl = environment.ssoLoginPageUrl;
  private jwtTokenQueryParamName = environment.urlTokenName;

  constructor(private actions$: Actions,
              private authService: AuthService,
              private urlUtil: UrlUtil,
              @Inject(DOCUMENT) private document: Document) {
  }

  @Effect()
  jwtLogin$ = this.actions$.pipe(
    ofType(JWT_LOGIN_ACTION_INIT),
    switchMap<JwtLoginActionInit, JwtLoginAction>(({goParams, jwtToken}) => {
      const queryParamToken = goParams.queryParams[this.jwtTokenQueryParamName];
      return this.authService.decodeToken(queryParamToken)
          .pipe(
            map(jwtAuthPayload => new JwtLoginActionSuccess(jwtAuthPayload, goParams, queryParamToken)),
            catchError(() => this.authService.decodeToken(jwtToken)
              .pipe(
                map(jwtAuthPayload => new JwtLoginActionSuccess(jwtAuthPayload, goParams, jwtToken)),
                catchError(() => of(new JwtLoginActionFail(goParams)))
              )
            ));
      }
    )
  );


  @Effect()
  jwtLoginSuccess$ = this.actions$.pipe(
    ofType(JWT_LOGIN_ACTION_SUCCESS),
    switchMap<JwtLoginActionSuccess, GoParams>(action => of(action.goParams)),
    map((goParams: GoParams) => new Go(goParams))
  );

  @Effect()
  jwtLoginFail$ = this.actions$.pipe(
    ofType(JWT_LOGIN_ACTION_FAIL),
    switchMap<JwtLoginActionFail, GoParams>(action => of(action.goParams)),
    map((goParams) => {
        const returnUriParams = this.urlUtil.removeJwtTokenFromUri(goParams);
        const strictUrl =  this.ssoLoginPageUrl + '?returnUrl='
          + URI.encode(this.document.location.origin + this.urlUtil.toUri(returnUriParams));
        return new Go({strictUrl});
      }
    )
  );

  @Effect()
  jwtCheck$ = this.actions$.pipe(
    ofType(JWT_CHECK_ACTION),
    switchMap<JwtCheckAction, RouterAction>(({goParams, jwtToken}) =>
      this.authService.decodeToken(jwtToken)
        .pipe(
          map(() => new Stay()),
          catchError(() => {
            const returnUriParams = this.urlUtil.removeJwtTokenFromUri(goParams);
            const strictUrl = this.ssoLoginPageUrl + '?returnUrl='
              + URI.encode(this.document.location.origin + this.urlUtil.toUri(returnUriParams));
            return of(new Go({strictUrl}));
          })
        )
    )
  );

  @Effect()
  jwtLogout$ = this.actions$.pipe(
    ofType(JWT_LOGOUT_ACTION),
    switchMap<JwtLogoutAction, RouterAction>(({redirectPath}) => {
      return of(new Go(redirectPath));
    })
  );

}
