import {createFeatureSelector, createSelector} from '@ngrx/store';
import {AuthState} from './auth.reducer';

export const getAuthState = createFeatureSelector<AuthState>('auth');
export const isAuthenticated = createSelector(getAuthState, state => {
  return state.authenticated;
});
export const getJwt = createSelector(getAuthState, state => {
  return state.jwt;
});

export const getPayload = createSelector(getAuthState, state => state.payload);
export const getUserDetails = createSelector(getPayload, state => state.userDetails);
export const getUserRoles = createSelector(getPayload, state => state.roles);
export const getUserScopes = createSelector(getPayload, state => state.scopes);
export const getLogin = createSelector(getUserDetails, state => state.login);
