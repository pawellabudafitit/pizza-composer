import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {AuthState} from '../store/auth.reducer';
import {Store} from '@ngrx/store';
import {getUserScopes} from '../store/auth.selector';
import {map, tap} from 'rxjs/operators';
import {NotificationAddAction} from '../../notification/store/notification.action';
import {NotificationType} from '../../notification/store/notification.model';

@Injectable()
export class ScopeGuard implements CanActivate {

  constructor(private store: Store<AuthState>) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> {
    const requiredRouteScopes = this.getRouteScopes(route);
    return this.store.select(getUserScopes).pipe(
      map((userScopes: string[]) => {
        switch (this.getOperator(route)) {
          case 'or':
            return requiredRouteScopes.some(requiredScope => userScopes.includes(requiredScope));
          case 'and':
            return requiredRouteScopes.every(requiredScope => userScopes.includes(requiredScope));
        }
      }),
      tap((activated) => {
        if (!activated) {
          this.store.dispatch(new NotificationAddAction({type: NotificationType.ERROR, message: 'User scope is insufficient.'}));
        }
      })
    );
  }

  getRouteScopes(route: ActivatedRouteSnapshot): string[] {
    return (route.data && route.data.scopes) ? route.data.scopes : [];
  }

  getOperator(route: ActivatedRouteSnapshot): string {
    return (route.data && ['or', 'and'].includes(route.data.scopesOperator)) ? route.data.scopesOperator : 'or';
  }
}
