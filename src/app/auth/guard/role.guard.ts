import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {AuthState} from '../store/auth.reducer';
import {Store} from '@ngrx/store';
import {getUserRoles} from '../store/auth.selector';
import {map, tap} from 'rxjs/operators';
import {NotificationAddAction} from '../../notification/store/notification.action';
import {NotificationType} from '../../notification/store/notification.model';

@Injectable()
export class RoleGuard implements CanActivate {

  constructor(private store: Store<AuthState>) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> {
    const requiredRouteRoles = this.getRouteRoles(route);
    return this.store.select(getUserRoles).pipe(
      map((userRoles: string[]) =>{
        switch (this.getOperator(route)) {
          case 'or':
            return requiredRouteRoles.some(requiredRole => userRoles.includes(requiredRole));
          case 'and':
            return requiredRouteRoles.every(requiredRole => userRoles.includes(requiredRole));
        }
      }),
      tap((activated) => {
        if (!activated) {
          this.store.dispatch(new NotificationAddAction({type: NotificationType.ERROR, message: 'User role is insufficient'}));
        }
      })
    );
  }

  getRouteRoles(route: ActivatedRouteSnapshot): string[] {
    return (route.data && route.data.roles) ? route.data.roles : [];
  }

  getOperator(route: ActivatedRouteSnapshot): string {
    return (route.data && ['or', 'and'].includes(route.data.rolesOperator)) ? route.data.rolesOperator : 'or';
  }
}
