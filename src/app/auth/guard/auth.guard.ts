import {Injectable} from '@angular/core';
import {CanActivate} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Store} from '@ngrx/store';
import {getAuthState} from '../store/auth.selector';
import {AuthFeatureState} from '../store/auth.reducer';
import {ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router/src/router_state';
import {JwtCheckAction, JwtLoginActionFail, JwtLoginActionInit} from '../store/auth.action';
import {UrlUtil} from '../../shared/utils/url.util';
import {map} from 'rxjs/operators';

@Injectable()
export class AuthGuard implements CanActivate {

  // private jwtTokenQueryParamName = environment.urlTokenName;
  constructor(private store: Store<AuthFeatureState>,
              private urlUtil: UrlUtil) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.store.select(getAuthState).pipe(
      map(({payload, authenticated, isAuthenticating, jwt}) => {
        if (authenticated !== undefined) {
          const goParams = this.urlUtil.convert(route);
          if (!authenticated) {
            if (goParams.queryParams && goParams.queryParams.token) {
              this.store.dispatch(new JwtLoginActionInit(goParams, goParams.queryParams.token));
            } else if (jwt) {
              this.store.dispatch(new JwtLoginActionInit(goParams, jwt));
            } else {
              this.store.dispatch(new JwtLoginActionFail(goParams));
            }
          } else {
            this.store.dispatch(new JwtCheckAction(goParams, jwt));
          }
          return authenticated;
        }
        return false;
      })
    );
  }

}

