import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {AuthState} from './store/auth.reducer';
import {Store} from '@ngrx/store';
import {getAuthState} from './store/auth.selector';
import {map, switchMap, tap} from 'rxjs/operators';
import {Injectable} from '@angular/core';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(private store: Store<AuthState>){}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.store.select(getAuthState).pipe(
      map(authState => authState.jwt ? req.clone({
        setHeaders: {
          Authorization: `Bearer: ${authState.jwt}`
        }
      }) : req),
      tap((request) => console.log(request)),
      switchMap((request) => next.handle(request))
    );
  }
}
