import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'sc-fake-auth',
  template: `
    <p>
      <a [routerLink]="['/']">Main</a>
    </p>
  `,
  styleUrls: ['./fake-auth.component.scss']
})
export class FakeAuthComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
