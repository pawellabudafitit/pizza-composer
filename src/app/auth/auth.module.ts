import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {StoreModule} from '@ngrx/store';
import {authReducer} from './store/auth.reducer';
import {EffectsModule} from '@ngrx/effects';
import {AuthEffects} from './store/auth.effect';
import {AuthService} from './service/auth.service';
import {FakeAuthComponent} from './containers/fake-auth/fake-auth.component';
import {RouterModule} from '@angular/router';
import {AuthGuard} from './guard/auth.guard';
import {RoleGuard} from './guard/role.guard';
import {ScopeGuard} from './guard/scope.guard';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [
    FakeAuthComponent
  ],
  exports: [
    FakeAuthComponent
  ]
})
export class AuthModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: RootAuthModule,
      providers: [AuthService, AuthGuard, RoleGuard, ScopeGuard]
    };
  }
}

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([]),
    StoreModule.forFeature('auth', authReducer),
    EffectsModule.forFeature([AuthEffects])
  ]
})
export class RootAuthModule {
}
