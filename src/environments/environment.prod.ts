export const environment = {
  production: true,
  urlTokenName: 'token',
  ssoLoginPageUrl: 'https://sso.example.com',
  backendUrl: 'http://localhost:3000'
};
