import {TestBed} from '@angular/core/testing';
import {ListService} from '../../app/list/list.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

describe('ListService', function () {

  const backendUrl = environment.backendUrl;

  let listService: ListService;

  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ], providers: [
        ListService
      ]
    });

    listService = TestBed.get(ListService);
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
  });

  it('should send request with page and size params', () => {
    listService.get({page: 1, size: 5}).subscribe(() => {});

    httpTestingController.expectOne({url: `${backendUrl}/list?page=1&size=5`});
  });

  it('should send request with page and size and one sort params', () => {
    const sort = [{property: 'name', asc: true}];
    listService.get({page: 1, size: 5, sort}).subscribe(() => {});

    httpTestingController.expectOne({url: `${backendUrl}/list?page=1&size=5&sort=name,asc`});
  });

  it('should send request with page and size and two sort params', () => {
    const sort = [{property: 'name', asc: true}, {property: 'description', asc: false}];
    listService.get({page: 1, size: 5, sort}).subscribe(() => {});

    httpTestingController.expectOne(
      {url: `${backendUrl}/list?page=1&size=5&sort=name,asc&sort=description,desc`});
  });

});
