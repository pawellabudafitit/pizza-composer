import {TestBed} from '@angular/core/testing';
import {Store, StoreModule} from '@ngrx/store';
import {ListState, ListType} from '../../../app/list/store/list.model';
import {routerReducerMap} from '../../../app/app-store/router/router.reducer';
import {initialListState, listReducer} from '../../../app/list/store/list.reducer';
import {list} from '../../../app/list/store/list.selector';
import {ListPage, ListPageSuccess} from '../../../app/list/store/list.action';

describe('ListPage', () => {

  let store: Store<ListState>;
  const currentState = {
    current: {
      page: 1,
      size: 5,
      listType: ListType.PAGED,
      items: [
        {id: 1}, {id: 2}, {id: 3}, {id: 4}, {id: 5}
      ],
      totalElements: 20,
      hasNext: true
    },
    next: null,
    loading: false
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({
          ...routerReducerMap,
          list: listReducer
        })
      ]
    });

    store = TestBed.get(Store);

    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should return list', function () {
    let result;

    store.select(list).subscribe(listState => result = listState);
    expect(result).toEqual(initialListState);

    store.dispatch(new ListPage());

    store.dispatch(new ListPageSuccess({
      items: [
        {id: 1},
        {id: 2},
        {id: 3},
        {id: 4},
        {id: 5}
      ],
      totalElements: 20,
      hasNext: true
    }));

    expect(result).toEqual(currentState);
  });
});
