import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';

import {Actions} from '@ngrx/effects';

import {cold, hot} from 'jasmine-marbles';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';

import {ListService} from '../../../app/list/list.service';
import {ListEffects} from '../../../app/list/store/list.effect';
import {ListPage, ListPageSuccess} from '../../../app/list/store/list.action';
import {provideMockActions} from '@ngrx/effects/testing';

describe('ListEffects', () => {
  let actions$: Observable<any>;
  let service: ListService;
  let effects: ListEffects;

  const response = {
    items: [
        { id: 1},
        { id: 2},
        { id: 3}
    ],
    totalElements: 20,
    hasNext: true
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        ListEffects,
        ListService,
        provideMockActions(() => actions$)
      ],
    });

    actions$ = TestBed.get(Actions);
    service = TestBed.get(ListService);
    effects = TestBed.get(ListEffects);

    spyOn(service, 'get').and.returnValue(of(response));
  });

  describe('load$', () => {
    it('should return a response from ListPageSuccess', () => {
      const action = new ListPage();
      const completion = new ListPageSuccess(response);

      actions$ = hot('-a', { a: action });
      const expected = cold('-b', { b: completion });

      expect(effects.load$).toBeObservable(expected);
    });
  });
});
