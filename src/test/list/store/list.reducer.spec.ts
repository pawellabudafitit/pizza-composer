import {initialListState, listReducer} from '../../../app/list/store/list.reducer';
import {ListPage, ListPageFail, ListPageSuccess} from '../../../app/list/store/list.action';
import {ListState, ListType} from '../../../app/list/store/list.model';

describe('List reducer', () => {
  it('should return default state when action undefined', function () {
    const state = listReducer(undefined, {} as any);

    expect(state).toEqual(initialListState);
  });

  it('should change state when list page action', function () {
    const action = new ListPage();
    const state = listReducer(undefined, action);

    expect(state).toEqual({
      current: initialListState.current,
      next: action.request,
      loading: true
    });
  });

  describe('ListPageSuccess', () => {

    let currentState: ListState;

    beforeEach(() => {
      currentState = {
        current: {
          page: 1,
          size: 5,
          listType: ListType.INFINITE,
          items: [
            {id: 1}, {id: 2}, {id: 3}, {id: 4}, {id: 5}
          ],
          totalElements: 20,
          hasNext: true
        },
        next: {
          page: 2,
          size: 5,
          listType: ListType.INFINITE
        },
        loading: true
      };
    });

    it('should add list items for infinite list', function () {
      const response = {
        items: [{id: 6}, {id: 7}, {id: 8}, {id: 9}, {id: 10}],
        totalElements: 20,
        hasNext: true
      };
      const state = listReducer(currentState, new ListPageSuccess(response));

      expect(state).toEqual({
        current: {
          page: 2,
          size: 5,
          listType: ListType.INFINITE,
          items: [
            {id: 1}, {id: 2}, {id: 3}, {id: 4}, {id: 5}, {id: 6}, {id: 7}, {id: 8}, {id: 9}, {id: 10}
          ],
          totalElements: 20,
          hasNext: true
        },
        next: null,
        loading: false
      });
    });

    it('should add list items for paged list', function () {
      currentState.next.listType = ListType.PAGED;
      const response = {
        items: [{id: 6}, {id: 7}, {id: 8}, {id: 9}, {id: 10}],
        totalElements: 20,
        hasNext: true
      };
      const state = listReducer(currentState, new ListPageSuccess(response));

      expect(state).toEqual({
        current: {
          page: 2,
          size: 5,
          listType: ListType.PAGED,
          items: [
            {id: 6}, {id: 7}, {id: 8}, {id: 9}, {id: 10}
          ],
          totalElements: 20,
          hasNext: true
        },
        next: null,
        loading: false
      });
    });
  });

  describe('ListPageFail', () => {
    it('should keep original state if loading page failed', function () {
      const currentState = {
        current: {
          page: 1,
          size: 5,
          listType: ListType.INFINITE,
          items: [
            {id: 1}, {id: 2}, {id: 3}, {id: 4}, {id: 5}
          ],
          totalElements: 20,
          hasNext: true
        },
        next: {
          page: 2,
          size: 5,
          listType: ListType.INFINITE
        },
        loading: true
      };

      const state = listReducer(currentState, new ListPageFail());

      expect(state).toEqual({
        current: currentState.current,
        next: null,
        loading: false
      });
    });
  });

});
