import {
  defaultRequest,
  LIST_PAGE,
  LIST_PAGE_FAIL,
  LIST_PAGE_SUCCESS,
  ListPage,
  ListPageFail,
  ListPageSuccess
} from '../../../app/list/store/list.action';
import {ListType} from '../../../app/list/store/list.model';

describe('List actions', () => {

  describe('ListPage', () => {
    it('should create ListPage action with default request', function () {
      const listPage = new ListPage();

      expect({...listPage}).toEqual({
        type: LIST_PAGE,
        request: defaultRequest
      });
    });
    it('should create ListPage action with custom request', function () {
      const request = {
        page: 2,
        size: 10,
        listType: ListType.INFINITE
      };
      const listPage = new ListPage(request);

      expect({...listPage}).toEqual({
        type: LIST_PAGE,
        request
      });
    });
  });

  describe('ListPageSuccess', () => {
    it('should create ListPageSuccess action', function () {
      const response = {
        items: [],
        totalElements: 20,
        hasNext: true
      };
      const listPageSuccess = new ListPageSuccess(response);

      expect({...listPageSuccess}).toEqual({
        type: LIST_PAGE_SUCCESS,
        response
      });
    });
  });

  describe('ListPageFail', () => {
    it('should create ListPageFail action', function () {
      const listPageFail = new ListPageFail();

      expect({...listPageFail}).toEqual({
        type: LIST_PAGE_FAIL
      });
    });
  });
});
