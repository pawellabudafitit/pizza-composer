const express = require('express');
const app = express();
const faker = require('faker');

const R = require('ramda');

// CORS
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  next();
});

class User {
  constructor(id, name, description){
    this.id=id;
    this.name=name;
    this.description=description;
  }
}
function deserializeSort(sort) {
  const deserialize = sortValue => {
    const sortField = sortValue.split(',');
    return {
      property: sortField[0],
      asc: 'asc' === sortField[1]
    };
  };
  if (Array.isArray(sort)) {
    return sort.map(deserialize);
  } else {
    return [deserialize(sort)];
  }
}
const list = Array.from({length:20}, (value, key) => new User(key+1, faker.name.title(), faker.name.jobDescriptor()));

app.get('/list', (req, res) => {
  const page = req.query.page || 1;
  const size = req.query.size || 5;
  let items = [...list];
  if(req.query.sort) {
    const sorts = deserializeSort(req.query.sort);
    R.reverse(sorts).forEach(sort => {
      const sortProp = R.prop(sort.property);
      const sortFn = sort.asc ? R.ascend(sortProp) : R.descend(sortProp);
      items = R.sort(sortFn, list);
      console.info(items);
    });
  }
  items = items.slice(Math.max(0,(page-1)*size), Math.min(page*size, 20));

  res.send({
    items,
    totalElements: 20,
    hasNext: page*size<20
  });
});

app.listen(3000, () => console.log('Mock application started'));
